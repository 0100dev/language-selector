Automatically changes the language for .COM and .NL. (NL forces Dutch and COM forces English) 


## License
Open-source under the GNU General Public Licence version 3 (GPLv3).