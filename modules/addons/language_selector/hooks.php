<?php
/**
 * Language Selector
 *
 * @file hook.php
 * @description WHMCS hook file
 * @author DevApp ("0100Dev")
 */

if (!defined('WHMCS'))
	die('This file cannot be accessed directly');

global $CONFIG, $whmcs, $_LANG;

if (isset($_SERVER['HTTP_HOST']) && !empty($_SERVER['HTTP_HOST'])) {
	$CONFIG['SystemURL'] = 'http://'.$_SERVER['HTTP_HOST'];
}

if (strpos($_SERVER['HTTP_HOST'], '.com') !== FALSE) {
	$whmcs->set_client_language('english');
} else {
	$whmcs->set_client_language('dutch');
}

//Language other then the language in the past? Update lang.
if ($_SESSION['Language'] != $vars['language']) {
	$whmcs->loadLanguage();
}