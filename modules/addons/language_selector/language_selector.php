<?php
/**
 * Language Selector
 *
 * @file language_selector.php
 * @description WHMCS "config" file
 * @author DevApp ("0100Dev")
 */

if (!defined('WHMCS'))
	die('This file cannot be accessed directly');
	
function language_selector_config() {

    $configarray = array(
    	'name' => 'Language Selector',
    	'description' => 'Switches between Dutch and English.',
    	'version' => '1.0',
    	'author' => '0100Dev',
    	'language' => 'dutch'
    );
	
    return $configarray;

}